import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	/*Lucas Zanlorensi 991500302
	 * 
	 * this class will be used to validate that a pw
	 * is 8 chars long and contains 2 digits*/
	@Test
    public void testHasUpperRegular() {
        PasswordValidator pv = new PasswordValidator();
        assertTrue("Password has no upper case", pv.hasUpper("TEST"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testHasUpperException() {
        PasswordValidator pv = new PasswordValidator();
        pv.hasUpper("");
        fail("Please enter a valid password");
    }

    @Test
    public void testHasUpperBoundaryIn() {
        PasswordValidator pv = new PasswordValidator();
        assertTrue("Password has no upper case", pv.hasUpper("tEsT"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testHasUpperBoundaryOut() {
        PasswordValidator pv = new PasswordValidator();
        pv.hasUpper("asdasdasdsaq");
        fail("Please enter a valid password");
    }
	
	@Test
	public void testHasValidDigitCountBoundaryOut() {
		boolean result = PasswordValidator.hasValidDigitCount("abcdef1a");
		assertFalse(result);
	}
	
	@Test
	public void testHasValidDigitCountBoundaryIn() {
		boolean result = PasswordValidator.hasValidDigitCount("abcdef12");
		assertTrue(result);
	}
	
	@Test
	public void testHasValidDigitCountExceptional() {
		boolean result = PasswordValidator.hasValidDigitCount("!@#$@#$%#$");
		assertFalse(result);
	}
	
	@Test
	public void testHasValidDigitCountRegular() {
		boolean result = PasswordValidator.hasValidDigitCount("abcdef12");
		assertTrue(result);
	}
	
	@Test
	public void testisValidLengthBoundaryIn() {
		boolean result = PasswordValidator.isValidLength("12345678");
		assertTrue(result);
	}
	
	@Test
	public void testisValidLengthBoundaryOut() {
		boolean result = PasswordValidator.isValidLength("1234567");
		assertFalse(result);
	}
	
	@Test
	public void testisValidLengthExceptional() {
		boolean result = PasswordValidator.isValidLength("12345");
		assertFalse(result);
	}
	
	@Test
	public void testisValidLengthRegular() {
		boolean result = PasswordValidator.isValidLength("123456789");
		assertTrue(result);
	}

}

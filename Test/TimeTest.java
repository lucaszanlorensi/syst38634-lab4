import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	//lucas zanlorensi
	
	@Test
	public void testGetMillisecondsBoundaryOut() {
		int seconds = Time.getMilliseconds("12:05:05:1000");
		assertTrue("Invalid number of milisseconds", seconds != 1000);
	}
	
	@Test
	public void testGetMillisecondsBoundaryIn() {
		int seconds = Time.getMilliseconds("12:05:05:999");
		assertTrue("Invalid number of milisseconds", seconds == 99);
	}
	
	@Test (expected=NumberFormatException.class )
	public void testGetMillisecondsException() {
		int seconds = Time.getMilliseconds("12:05:05:0T");
	}
	
	@Test
	public void testGetMilliseconds() {
		int seconds = Time.getMilliseconds("12:05:05:05");
		assertTrue("Invalid number of milisseconds", seconds == 5);
	}
	


}

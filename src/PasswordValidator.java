
public class PasswordValidator {

	/*Lucas Zanlorensi 991500302
	 * 
	 * this class will be used to validate that a pw
	 * is 8 chars long and contains 2 digits*/
	
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGITS = 2;
	
	public static boolean isValidLength(String password) {
		return password.length() >= MIN_LENGTH;
	}
	
	public static boolean hasValidDigitCount(String password) {
		int digitscount = 0;
		for(int i = 0; i < password.length(); i++) {
			if(Character.isDigit(password.charAt(i)))
				digitscount++;
		}
		return digitscount >= MIN_DIGITS;
	}
	
	public boolean hasUpper(String password) throws IllegalArgumentException{
        char[] passChars = password.toCharArray();
        for (char c: passChars) {
            if(Character.isUpperCase(c))
                return true;
        }
        throw new IllegalArgumentException("No uppercase found");
    }
}
